import './App.css';
import React from "react";

function App() {
  return (
    React.createElement('div', null,
      React.createElement('div', null,
        <label className='switch' htmlFor='checkbox'>
          <input type='checkbox' id='checkbox'/>
          <div className='slider round' onClick={changeTheme} />
        </label>),
      React.createElement('h1', {style: {color: '#999', fontSize: '19px'}}, 'Solar system planets:'),
      <ul className='planets-list'>
        <li>Mercury</li>
        <li>Venus</li>
        <li>Earth</li>
        <li>Mars</li>
        <li>Jupiter</li>
        <li>Saturn</li>
        <li>Uranus</li>
        <li>Neptune</li>
      </ul>
    )
  );
}

function changeTheme() {
  document.querySelector('body').classList.toggle('dark');
  document.querySelector('.planets-list').classList.toggle('dark');
}

export default App;
